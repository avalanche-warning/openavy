#'Create sno files for SNOWPACK simulations
#'
#'@usage get_sno()
#'
#'@param pois list of points of interest
#'@param latlon list of latitude and longitude
#'@param SlopeAzi list of Slope Azi (aspects)
#'@param SlopeAngle list of Slope Angles
#'@param path_sno directory where the sno files are stored
#'
#'@export get_sno
#'
#'@return sno files
#'
#'@import awsome.smet
#'
#'@examples get_sno(pois = pois,
#'.                 latlon = latlon,
#'.                 path_sno = "/path/where/sno/files/are/stored",
#'                  SlopeAzi = SlopeAzi)

get_sno <- function(pois, latlon, path_sno = getwd(), SlopeAzi = SlopeAzi, SlopeAngle = SlopeAngle) {


  columns <- c("poi", "lat", "lon", "timestamp", "TA", "RH", "TSG", "VW", "DW", "ISWR", "PSUM")
  df_nwp <- data.frame(matrix(nrow= 0, ncol = length(columns)))
  colnames(df_nwp) = columns

  for (x in 1:length(pois)) {
    #print(pois[[x]])

    altitude <- strsplit(pois[[x]], "_")

    smet <- as(df_nwp, "SMET")
    smet <- set.header(smet, list("station_id" = pois[[x]],
                                  "station_name" = pois[[x]],
                                  "longitude"= latlon[[x]][2],
                                  "latitude"= latlon[[x]][1],
                                  "altitude" = altitude[[1]][2],
                                  "nodata" = -999,
                                  "tz" = 0,
                                  "source" = "lwd",
                                  "ProfileDate" = "2024-09-09",
                                  "HS_Last" = 0.0000,
                                  "SlopeAngle" = SlopeAngle[[x]],
                                  "SlopeAzi" = SlopeAzi[[x]],
                                  "nSoilLayerData" = 0,
                                  "nSnowLayerData" = 0,
                                  "SoilAlbedo" = 0.09,
                                  "BareSoil_z0" = 0.200,
                                  "CanopyHeight" = 0.00,
                                  "CanopyLeafAreaIndex" = 0.00,
                                  "CanopyDirectThroughfall" = 1.00,
                                  "WindScalingFactor" = 1.00,
                                  "ErosionLevel" = 0,
                                  "TimeCountDeltaHS" = 0.000000,
                                  "fields" ="timestamp Layer_Thick  T  Vol_Frac_I  Vol_Frac_W  Vol_Frac_V  Vol_Frac_S Rho_S Conduc_S HeatCapac_S  rg  rb  dd  sp  mk mass_hoar ne CDot metamo"
    ))

    smet <- set.station_id(smet, pois[[x]])
    write.smet(smet, smetfile = paste0(path_sno, pois[[x]] ,".sno"))
  }
}
