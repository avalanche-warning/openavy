#'Run a SNOWPACK simulation and get pro files
#'
#'@usage get_pro()
#'
#'@param path_snowpack directory where SNOWPACK is installed
#'@param path_ini directory where the iniFiles are stored
#'@param end enddate of your simulation in ISO8601 format
#'@param stations list of stations
#'@param dates list of dates
#'
#'@export get_pro
#'
#'@import lubridate
#'
#'@return SNOWPACK simulations
#'
#'@examples get_pro(path_snowpack = "snowpack -b ",
#'                  path_ini = "/path/to/your/ini/directory",
#'                  end = end,
#'                  stations = stations,
#'                  dates = dates)
get_pro <- function(path_snowpack = "/Applications/Inishell/inishell-apps/Snowpack/bin/snowpack -b  ",
                    path_ini = getwd(), end, stations, dates) {
  dates <- ymd(dates) + days(1)
  stations <- unlist(stations)
  dates <- unlist(dates)
  path_snowpack <- path_snowpack
  for (y in 1:length(stations)) {
    system(paste0(path_snowpack, dates[y] ,'T00:00:00 -e ', end ,'T00:00:00 -c "', path_ini, stations[y], ".ini", '"'))
  }
}

