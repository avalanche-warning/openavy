#'Load list of maximum grainsizes from from observed profiles in caaml format
#'
#'@usage load_gsizemax_caaml()
#'
#'@param obs_profiles list of observed profiles
#'@param gsize observed profile grain sizes
#'
#'@export load_gsizemax_caaml
#'
#'@return list of max observed grain sizes
#'
#'@examples gsize_max <- load_gsizemaxcaaml(obs_profiles = obs_profiles, gsize = gsize)
load_gsizemax_caaml <- function(obs_profiles = obs_profiles, gsize = gsize){
  gsize <- list()
  for (g in 1:length(obs_profiles)){
    gsize[[g]] <- as.numeric(obs_profiles[[g]]$layers$gsize)
  }
  gsize_max <- list()
  for (g in 1:length(obs_profiles)){
    gsize_max[[g]] <- as.numeric(obs_profiles[[g]]$layers$gsize)
  }
  for (m in 1:length(obs_profiles)){
    gsize_max[[m]] <- max(na.omit(unlist(gsize[m])))
  }
  return(gsize_max)
}
