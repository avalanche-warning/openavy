#'Load SlopeAngle (aspect) from a geojson file
#'
#'@usage load_angle_geojson()
#'
#'@param path_geojson geojson filepath
#'@param SlopeAngle name of SlopeAngle column in geojson file
#'
#'@export load_angle_geojson
#'
#'@return list of SlopeAngle
#'
#'@import jsonlite
#'
#'@examples SlopeAngle <- load_angle_geojson(path_geojson = "/path/to/your/geojson",
#'                                       SlopeAngle = SlopeAngle)
load_angle_geojson <- function(path_geojson = getwd(), SlopeAngle = SlopeAngle){
  SlopeAngle <- fromJSON(path_geojson, simplifyVector = TRUE, flatten = TRUE)
  SlopeAngle_df <- as.data.frame(SlopeAngle, cut.names = TRUE, fix.empty.names = FALSE)
  names(SlopeAngle_df) <- gsub("features.properties.","",names(SlopeAngle_df))
  SlopeAngle <- as.list(SlopeAngle_df$SlopeAngle)
}
