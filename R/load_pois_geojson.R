#'Load points of interest from a geojson file
#'
#'@usage load_pois_geojson()
#'
#'@param path_geojson geojson filepath
#'@param POI name of point of interest column in geojson file
#'
#'@export load_pois_geojson
#'
#'@return list of points of interest
#'
#'@import jsonlite
#'
#'@examples pois <- load_pois_geojson("/path/to/your/geojson",
#'                                    POI = POI)
load_pois_geojson <- function(path_geojson = getwd(), POI = POI){
  pois <- fromJSON(path_geojson, simplifyVector = TRUE, flatten = TRUE)
  pois_df <- as.data.frame(pois, cut.names = TRUE, fix.empty.names = FALSE)
  names(pois_df) <- gsub("features.properties.","",names(pois_df))
  pois <- as.list(unique(pois_df$POI))
}
