
<!-- README.md is generated from README.Rmd. Please edit that file -->

# openavy

The goal of openavy is to provide a package for R that lets you run
[SNOWPACK](https://snowpack.slf.ch) simulations.

Simulations are driven with open nwp data and can be initialized

1.  with observed snow profiles
2.  purely with open nwp data

## Installation

You can install the development version of openavy like so:

``` r
remotes::install_gitlab("avalanche-warning/snow-cover/model-chains/openavy")
```

## Examples

This is a basic example which shows you how to import dates, coordinates
and station names from observed snowprofiles in caaml format.

``` r
obs_profiles <- load_obs_profiles_caaml(path_caaml = "/path/to/your/caaml/files/")
dates <- load_dates_caaml(obs_profiles = obs_profiles, dates = dates)
stations <- load_stations_caaml(obs_profiles = obs_profiles, stations = stations)
latlon <- load_latlon_caaml(obs_profiles = obs_profiles, latlon = latlon)
```

This is a basic example which shows you how to import dates, coordinates
and POI/station names from a geojson file.

``` r
geojson <- "/path/to/your/geojson/file.geojson"
pois <- load_pois_geojson(path_geojson = geojson, POI = POI)
stations <- load_pois_geojson(path_geojson = geojson, POI = POI)
latlon <- load_latlon_geojson(path_geojson = geojson, coordinates = coordinates)
dates <- load_dates_geojson(path_geojson = geojson, date = date)
SlopeAzi <- load_azi_geojson(path_geojson = geojson, SlopeAzi = SlopeAzi)
SlopeAngle <- load_angle_geojson(path_geojson = geojson, SlopeAngle = SlopeAngle)
```

This is a basic example which shows you how to create basic iniFiles

``` r
library(openavy)
get_ini(meteopath_in = "/path/to/your/smetfiles",
         snowpath_in = "/path/to/your/caaml",
         meteopath_out = "/path/to/your/output/directory",
         path_ini = "/path/to/your/iniFiles",
         stations = stations)
```

This is a basic example which shows you how to create iniFiles for
[SNOWPACK](https://snowpack.slf.ch) simulations initialized purely with
open nwp data

``` r
library(openavy)
get_ini_poi(meteopath_in = "/path/to/your/smetfiles",
           snowpath_in = "/path/to/your/caaml",
           meteopath_out = "/path/to/your/output/directory",
           path_ini = "/path/to/your/iniFiles"
           pois = pois)
```

This is a basic example which shows you how to create .sno files for
[SNOWPACK](https://snowpack.slf.ch) simulations initialized purely with
open nwp data

``` r
library(openavy)
get_sno(pois = pois,
        latlon = latlon,
        SlopeAzi = SlopeAzi,
        SlopeAngle = SlopeAngle, 
        path_sno = "/path/to/your/sno/files/")
```


This is a basic example which shows you how to run
[SNOWPACK](https://snowpack.slf.ch) simulations and obtain .pro files

``` r
library(openavy)
get_pro(path_snowpack = "snowpack -b ", 
        path_ini = "/path/to/your/ini/directory", 
        end = end, 
        stations = stations, 
        dates = dates)
```

## Option 1: [SNOWPACK](https://snowpack.slf.ch) simulations driven with open nwp data and initialized with observed snow profiles

So let’s say your username is avy and you want to initialize
[SNOWPACK](https://snowpack.slf.ch) simulations with observed
snowprofiles that were observed on the dates 2024-08-25 and 2024-08-26
at two stations at altitudes 2200 m and 2530 m.

Your **snowpack** directory tree looks like this:

``` r
├── ini
├── input
│   ├── meteo
│   ├── snow
│       ├── station_2200.caaml
│       ├── station_2530.caaml
├── output
```

**Step 1**

Save the test caaml files from
[docs](https://gitlab.com/avalanche-warning/snow-cover/model-chains/openavy/-/tree/main/docs)
to “/Users/avy/snowpack/input/snow/”.

**Step 2**

start by innstalling and loading the package
[remotes](https://cran.r-project.org/web/packages/remotes/index.html)
and then use it to install and load all the required AWSOME packages:

``` r
install.packages("remotes")
library(remotes)
install_gitlab("avalanche-warning/universal/packages/awsome-r/awsome.smet")
library(awsome.smet)
install_gitlab("avalanche-warning/data/fetchers/opensmeteo")
library(opensmeteo)
install_gitlab("avalanche-warning/snow-cover/model-chains/openavy")
library(openavy)
```

**Step 3** Define the forecast period as **end**. In this case we want a
forecast for the next 2 days.

``` r
end <- format(today() + 2, "%Y-%m-%d")
```

**Step 4**

Import metadata from caaml files

``` r
obs_profiles <- load_obs_profiles_caaml(path_caaml = "/path/to/your/caaml/files/")
dates <- load_dates_caaml(obs_profiles = obs_profiles, dates = dates)
stations <- load_stations_caaml(obs_profiles = obs_profiles, stations = stations)
latlon <- load_latlon_caaml(obs_profiles = obs_profiles, latlon = latlon)
```

**Step 5**

Retrieve open numerical weather prediction data in SMET file format with
the get_smet function from
[opensmeteo](https://gitlab.com/avalanche-warning/data/fetchers/opensmeteo).
Change path according to your needs.

``` r
get_smet_fc(dates = dates, end = end, path_smet = "/Users/avy/input/meteo/",
            stations = stations, latlon = latlon)
```

**Step 6**

Create iniFiles. Change paths accoridng to your needs.

``` r
get_ini(meteopath_in = "/Users/avy/snowpack/input/meteo/", 
        snowpath_in = "/Users/avy/snowpack/input/snow/", 
        meteopath_out = "/Users/avy/snowpack/output/"
        path_ini = "/Users/avy/snowpack/ini/",
        stations = stations)
```

**Step 7**

Run [SNOWPACK](https://snowpack.slf.ch) simulations. Change
path_snowpack to the directory where you installed
[SNOWPACK](https://snowpack.slf.ch). Please note that after the
intsllation path of [SNOWPACK](https://snowpack.slf.ch) you need to add
`"-b "`.

``` r
get_pro(path_snowpack = "/Applications/Inishell/inishell-apps/Snowpack/bin/snowpack -b  ", 
        path_ini = "/Users/avy/snowpack/ini/", 
        end = end, 
        stations = stations, 
        dates = dates)
```

## Option 2: [SNOWPACK](https://snowpack.slf.ch) simulations driven with open nwp data and initialized purely with open nwp data

So let’s say your username is avy and you want to initialize
[SNOWPACK](https://snowpack.slf.ch) simulations purely with open nwp
data for a number of points of interest that are stored in a geojson
file.

Your **snowpack** directory tree looks like this:

``` r
├── ini
├── input
│   ├── meteo
│   ├── snow
├── output
├── geojson
├── df
```

**Step 1**

Save your geojson files with your points of interest to
`"/Users/avy/snowpack/geojson/poi_example.geojson"`.

**Step 2**

Innstall and load the package
[remotes](https://cran.r-project.org/web/packages/remotes/index.html)
and then use it to install and load all the required AWSOME packages:

``` r
install.packages("remotes")
library(remotes)
install_gitlab("avalanche-warning/universal/packages/awsome-r/awsome.smet")
library(awsome.smet)
install_gitlab("avalanche-warning/data/fetchers/opensmeteo")
library(opensmeteo)
install_gitlab("avalanche-warning/snow-cover/model-chains/openavy")
library(openavy)
```

**Step 3** Define the forecast period as **end**. In this case we want a
forecast for the next 2 days.

``` r
end <- format(today() + 2, "%Y-%m-%d")
```

**Step 4**

Import metadata from geojson file

``` r
geojson <- "/Users/avy/snowpack/geojson/poi_example.geojson"
pois <- load_pois_geojson(path_geojson = geojson, POI = POI)
stations <- load_pois_geojson(path_geojson = geojson, POI = POI)
latlon <- load_latlon_geojson(path_geojson = geojson, coordinates = coordinates)
dates <- load_dates_geojson(path_geojson = geojson, date = date)
SlopeAzi <- load_azi_geojson(path_geojson = geojson, SlopeAzi = SlopeAzi)
SlopeAngle <- load_angle_geojson(path_geojson = geojson, SlopeAngle = SlopeAngle)
```

**Step 5**

Retrieve open numerical weather prediction data in SMET file format with
the get_smet function from
[opensmeteo](https://gitlab.com/avalanche-warning/data/fetchers/opensmeteo).
Change path according to your needs.

**Note: stations = pois**

``` r
get_smet_poi(path_smet = "/Users/avy/snowpack/input/meteo",
             stations = pois, 
             latlon = latlon, 
             path_df = "/Users/avy/snowpack/df/")
```




**Step 6**

Create iniFiles. Change paths accoridng to your needs.

``` r
get_ini_poi(meteopath_in = "/Users/avy/snowpack/input/meteo/", 
            snowpath_in = "/Users/avy/snowpack/input/snow/", 
            meteopath_out = "/Users/avy/snowpack/output/"
            path_ini = "/Users/avy/snowpack/ini/",
            pois = pois)
```

**Step 7**

Creat .sno files. Change paths accoridng to your needs.

``` r
get_sno(pois = pois,
        latlon = latlon,
        SlopeAzi = SlopeAzi,
        SlopeAngle = SlopeAngle, 
        path_sno = "/Users/avy/snowpack/input/snow/")
```


**Step 8**

Run [SNOWPACK](https://snowpack.slf.ch) simulations. Change
path_snowpack to the directory where you installed
[SNOWPACK](https://snowpack.slf.ch). Please note that after the
intsllation path of [SNOWPACK](https://snowpack.slf.ch) you need to add
`"-b "`.

**Note: stations = pois**

``` r
get_pro(path_snowpack = "/Applications/Inishell/inishell-apps/Snowpack/bin/snowpack -b  ", 
        path_ini = "/Users/avy/snowpack/ini/", 
        end = end, 
        stations = pois, 
        dates = dates)
```
